<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

use App\Task;

class TaskController extends Controller
{
    public function index(){
        $tasks = Auth::user()->tasks->where("iscompleted", false);
        $completed_tasks = Auth::user()->tasks->where("iscompleted", true);
        return view("home", compact("tasks", "completed_tasks"));
    }
    public function store(Request $request)
    {
        $input = $request->all();
        $task = new Task();
        $task->task = request("task");
        $task['user_id'] = Auth::user()->id;
        $task->save();
        return Redirect::back()->with("message", "Task has been added");
    }
    public function complete($id)
    {
        $task = Task::find($id);
        $task->iscompleted = true;
        $task->save();
        return Redirect::back()->with("message", "Task has been added to completed list");
    }
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->delete();
        return Redirect::back()->with('message', "Task has been deleted");
    }
    public function profile()
    {
        return view('profile');

    }
}
